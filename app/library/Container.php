<?php namespace Vokuro;

class Container {
    public $ticket;
    public $CSRFPreventionToken;

    private $api_url;

    public function __construct() {
        $this->api_url = getenv('proxmox_url') . '/api2/json';
    }

    public function start($id) {
        $r = new Request($this->api_url . '/nodes/s129183/lxc/' . $id . '/status/start');
        $r->setHeader('CSRFPreventionToken', $this->CSRFPreventionToken);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->ticket);
        return $r->post([]);
    }

    public function stop($id) {
        $r = new Request($this->api_url . '/nodes/s129183/lxc/' . $id . '/status/shutdown');
        $r->setHeader('CSRFPreventionToken', $this->CSRFPreventionToken);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->ticket);
        return $r->post([]);
    }

    private function getNextId() {
        $r = new Request($this->api_url . '/cluster/nextid');
        $r->setHeader('CSRFPreventionToken', $this->CSRFPreventionToken);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->ticket);
        return $r->get();
    }

    public function create($data) {
        $r = new Request($this->api_url . '/nodes/s129183/lxc');
        $r->setHeader('CSRFPreventionToken', $this->CSRFPreventionToken);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->ticket);

        $data['swap'] = $data['mem'];

        $current_resources = $this->get();
        $current_ids = [];
        foreach($current_resources as $resource) {
            if($resource['type'] == 'lxc') {
                $current_ids[] = explode('/', $resource['id'])[1];
            }
        }


        $vmid = $this->getNextId()['data'];

        $ip = '10.0.0.' . (substr($vmid, 1) + 10) . '/8';

        $password = bin2hex(random_bytes(14));

        $payload = [
            'ostemplate' => 'local:vztmpl/ubuntu-16.04-x86_64.tar.gz',
            'vmid' => $vmid,
            'rootfs' => 'HDD:' . $data['disk'],
            'hostname' => $data['hostname'],
            'memory' => $data['mem'],
            'swap' => $data['swap'],
            'cores' => $data['cpu'],
            'net0' => 'name=eth0,bridge=vmbr0,gw=10.0.0.1,ip=' . $ip,
            'pool' => 'test',
            'password' => $password
        ];

        $ret = $r->post($payload);
        $ret['password'] = $password;
        $ret['ip'] = $ip;
        $ret['started'] = $this->start($vmid);;

        return $ret;

    }

    public function get() {
        $r = new Request($this->api_url . '/cluster/resources');
        $r->setHeader('CSRFPreventionToken', $this->CSRFPreventionToken);
        $r->setHeader('Cookie', 'PVEAuthCookie='.$this->ticket);
        $resources = $r->get();

        return $resources['data'];
    }
}

