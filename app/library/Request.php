<?php namespace Vokuro;

class Request {

    protected $headers = [];
    protected $ch;

    public function __construct($url) {
        $this->setup($url);
    }

    public function setHeader($name, $value) {
        $this->headers[$name] = $value;
    }

    public function setup($url) {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }

    public function call() {
        $formatted_headers = [];

        foreach($this->headers as $name => $header) {
            $formatted_headers[] = $name . ': ' . $header;
        }

        if(!empty($formatted_headers)) {
            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $formatted_headers);
        }

        $out = curl_exec($this->ch);

        if(empty($out)) {

        }

        return json_decode($out, true);
    }

    public function get() {
        return $this->call();
    }

    public function post($data) {
        curl_setopt($this->ch,CURLOPT_POST, 1);
        curl_setopt($this->ch,CURLOPT_POSTFIELDS, http_build_query($data));
        return $this->call();
    }
}

