<?php
namespace Vokuro\Controllers;

use Phalcon\Http\Request as PRequest;
use Vokuro\Request;
use Phalcon\Mvc\Controller;


/**
 * Display the default index page.
 */
class BaseController extends Controller {

    public $api_url = '';
    private $authenticated = false;

    public function initialize() {
        session_start();
        $this->authenticated = !empty($_SESSION['auth']);
        $this->api_url = getenv('proxmox_url') . '/api2/json';
        $this->view->authenticated = $this->authenticated;
    }

}