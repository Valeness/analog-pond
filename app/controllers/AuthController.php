<?php
namespace Vokuro\Controllers;

use Phalcon\Http\Request as PRequest;
use Vokuro\Request;

/**
 * Display the default index page.
 */
class AuthController extends BaseController {

    public $api_url = '';


    public function indexAction() {

    }

    public function authenticateAction() {
        $this->api_url = getenv('proxmox_url') . '/api2/json';
        $Request = new PRequest();
        $data = $Request->getPost();

        $r = new Request($this->api_url . '/access/ticket');
        $retval = $r->post([
            'username' => $data['username'] . '@pam',
            'password' => $data['password']
        ]);

        if($retval['data'] === null) {
            $retval['status'] = 1;
        } else {
            $_SESSION['auth'] = $retval['data'];
        }

        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($retval));
        return $response;
    }
}
