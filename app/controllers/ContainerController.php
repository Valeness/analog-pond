<?php
namespace Vokuro\Controllers;

use Vokuro\Container;
use Phalcon\Mvc\View;

class ContainerController extends BaseController {


    private $configs = [
        ['mem' => '512',
        'cpu' => 1,
        'disk' => '10'],
        ['mem' => '1024',
            'cpu' => 2,
            'disk' => '20'],
        ['mem' => '2048',
            'cpu' => 4,
            'disk' => '35']
    ];

    private function formatBytes($bytes, $precision = 2) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        //$bytes /= pow(1024, $pow);
        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];

    }

    public function activateAction() {
        $container = new Container();
        $container->ticket = $_SESSION['auth']['ticket'];
        $container->CSRFPreventionToken = $_SESSION['auth']['CSRFPreventionToken'];

        $retval = $container->start(explode('/', $_POST['id'])[1]);

        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($retval));
        return $response;
    }
    public function deactivateAction() {
        $container = new Container();
        $container->ticket = $_SESSION['auth']['ticket'];
        $container->CSRFPreventionToken = $_SESSION['auth']['CSRFPreventionToken'];

        $retval = $container->stop(explode('/', $_POST['id'])[1]);

        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($retval));
        return $response;
    }

    public function createAction() {
        $container = new Container();
        $container->ticket = $_SESSION['auth']['ticket'];
        $container->CSRFPreventionToken = $_SESSION['auth']['CSRFPreventionToken'];
        $payload = [
            'hostname' => $_POST['hostname']
        ];

        $payload = array_merge($payload, $this->configs[$_POST['grain_select']]);

        $retval = $container->create($payload);

        $response = new \Phalcon\Http\Response();
        $response->setContent(json_encode($retval));
        return $response;
    }

    public function getAction() {
        $this->api_url = getenv('proxmox_url') . '/api2/json';
        $containers = new Container();
        $containers->ticket = $_SESSION['auth']['ticket'];
        $containers->CSRFPreventionToken = $_SESSION['auth']['CSRFPreventionToken'];
        $containers = $containers->get();

        if($_GET['dump']) {
            echo '<pre>';
            var_dump($containers);
            exit;
        }

        foreach($containers as $k => $container) {
            if($container['type'] !== 'lxc') {
                unset($containers[$k]);
            } else {
                if(!empty($container['maxmem'])) {
                    $containers[$k]['formatted_mem'] = $this->formatBytes($container['maxmem']);
                }
            }
        }
        usort($containers, function($a, $b) {
            return strcmp($a['id'], $b['id']);
        });

        return $this->view->render('container', 'get', [
            'containers' => $containers,
            'grains' => [
                [
                    'cpu' => '1',
                    'mem' => '512 MB',
                    'disk' => '10 GB'
                ],
                [
                    'cpu' => '2',
                    'mem' => '1 GB',
                    'disk' => '20 GB'
                ],
                [
                    'cpu' => '2',
                    'mem' => '2 GB',
                    'disk' => '35 GB'
                ],
            ]
        ]);

    }
}
