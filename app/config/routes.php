<?php
/*
 * Define custom routes. File gets included in the router service definition.
 */
$router = new Phalcon\Mvc\Router();

$router->add('/', [
    'controller' => 'index',
    'action' => 'index'
]);

$router->add('/auth', [
    'controller' => 'auth',
    'action' => 'authenticate'
]);

//$router->add('/containers', [
//    'controller' => 'container',
//    'action' => 'get'
//]);

return $router;
